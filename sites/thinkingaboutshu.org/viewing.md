## Viewing Recommendations

* [Shuhua's predebut self-introduction](https://www.youtube.com/watch?v=Dwi-oX-hRg8)
* [Playlist: (G)I-DLE I-TALK](https://www.youtube.com/watch?v=MOL71EcqTh8&list=PLG8DKAYW6M258LY8L2Pu0rVPiS5n1HJtU)
* [Playlist: (G)I-DLE To Neverland](https://www.youtube.com/watch?v=08ATpBqlAIk&list=PLQv1U2oSD8oAse_JRSp2P63L5mgKTUB8S)
* (G)I-DLE K-LOG [EP.1](https://www.youtube.com/watch?v=mP71e4hFfL0) [Ep.2](https://www.youtube.com/watch?v=DCzC6tbIcyg)
* [Playlist: (G)I-DLE #HASHTALK](https://www.youtube.com/playlist?list=PLG8DKAYW6M27QwRH09xiI8Zel7F1pmOS4)
* [Rhythmic gymnastics, 190205 ](https://www.youtube.com/watch?v=GmaPL6maAtY)
* Soojin, Shuhua cooking [Part 1](https://www.vlive.tv/video/1347641) [Part 2](https://www.vlive.tv/video/1347842)
* [V LIVE](https://channels.vlive.tv/CE2621/home), [text list](vlist) of (G)I-DLE V LIVEs you can use with youtube-dl
* [(G)I-DLE X CouchTalk, 190227](https://www.vlive.tv/video/114294)
* [(G)I-DLE X Liev, 190706](https://www.vlive.tv/video/137608)
* [(G)I-DLE X Liev, 200810](https://www.vlive.tv/video/207242)
* Super TV2 Ep. 6
* Idol Room Ep. 41, 46
* Weekly Idol Ep. 413, 458
* Mr. Player with Shuhua [20210605](https://www.youtube.com/watch?v=HThGznwjEig) [20210612](https://www.youtube.com/watch?v=AGIWf7LHHnQ)
* Playlist: [i'M THE TREND with Minnie and Soojin](https://www.youtube.com/playlist?list=PL72zKS6N-IObk46kjeFFEdTXhkzZd09lu)
* Playlist: [Learn Way with Yuqi](https://www.youtube.com/playlist?list=PL6NUNGdv0v-E7JuMIHRk0NL8YU-8SCZO0)
* Playlist: [Secret Folder](https://www.youtube.com/playlist?list=PL6NUNGdv0v-FyTCeAFRrecrJb5XQ9WWRu)
* Playlist: [Idol Ground](https://www.youtube.com/playlist?list=PLvzCmZ9uM9--grDEDhceACR0wdcSlVwWo)
* Playlist: [Never-ending Neverland](https://www.youtube.com/playlist?list=PLG8DKAYW6M27l2MbN-7iafsW9z4G6VXOR)

![Shu](image_7319978791588744533354.jpg)
