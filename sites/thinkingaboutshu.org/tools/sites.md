## Useful Websites

### [Tweetdeck](https://tweetdeck.twitter.com)

Monitor multiple Twitter hashtags or timelines at once

Protip: don't run this in the background, it's a resource hog  

#### Simpler(?) alternative to Tweetdeck

You can open your browser in a script sort of like this.

    #!/bin/sh
    
    search="https://twitter.com/search?q="
    ht="%23"
    lang="lang:ko"
    exclude="-RT -© -cr:"
    live="&f=live"
    
    tabs="https://twitter.com/home
    $search$ht슈화 $lang $exclude$live
    $search$ht여자아이들 OR $ht미연 OR $ht민니 OR $ht수진 OR $ht소연 OR $ht우기 OR $ht슈화 $lang $exclude$live"
    
    tabs=$(echo "$tabs" | sed 's/.*/"&"/' | tr '\n' ' ')
    
    eval $BROWSER $tabs

Bookmarking such searches solves this also. (And is less goofy.) Unlike
Tweetdeck neither requires a login.

    https://twitter.com/search?q=%23슈화 filter%3Amedia -RT -© -cr%3A -credits -via -upchar -chara -data -%23Roleplay&src=typed_query&f=live

See the [official
documentation](https://developer.twitter.com/en/docs/twitter-api/v1/rules-and-filtering/search-operators)
on operators for advanced Twitter searches.

### [Kastden's Selca](https://selca.kastden.org/kpop)

Idol social media scraper and archive

### [JPopsuki](https://jpopsuki.eu)

Private tracker for Asian music

### [AvistaZ](https://avistaz.to)

Private tracker for Asian audio/video

### Deezer

Although you would have to be insane to use this service
normally, tools such as [deemix](https://deemix.net/)
or [Freezer](https://www.freezer.life/) are highly convenient for
downloading music.

### [kpopstan.com](https://kpopstan.com/)

Another website to download Kpop music from.

![Shu](../EVJZl6tUUAAPoEe.jpg)
