## Browsers and Extentions

* [qutebrowser](https://qutebrowser.org/) - QtWebengine (i.e. Chromium) based, Vim-style key bindings, sane defaults, configuration in Python, Greasemonkey script support and support for Brave's adblocker via python-adblock
* [redirect\_image\_to\_original.user.js](https://gitlab.com/loopvid/scripts/blob/master/userscripts/redirect_image_to_original.user.js) - Redirect images to their highest quality version, [fork](https://gitlab.com/ysh16/dotfiles/-/blob/master/.config/qutebrowser/greasemonkey/redirect_image_to_original.user.js)
* [Old Reddit Please!](https://greasyfork.org/en/scripts/40897-old-reddit-please) - If you need to use Reddit, this might make it more bearable, [fork](https://gitlab.com/ysh16/dotfiles/-/blob/master/.config/qutebrowser/greasemonkey/Old%20Reddit%20Please!.user.js)

## Programs

* [youtube-dl](https://ytdl-org.github.io/youtube-dl/) - Download videos from YouTube, V LIVE, etc.
* [mpv](https://mpv.io/) - Media player that plays local files amazingly well but also YouTube, V LIVE, etc. via youtube-dl
* [sxiv](https://github.com/muennich/sxiv) - Image viewer and browser for X Windows, keybindings to do whatever can be easily added in a [shell script](https://gitlab.com/ysh16/dotfiles/-/blob/master/.config/sxiv/exec/key-handler) and the rest is very easily configurable by editing the source
* [boram](https://github.com/Kagami/boram) - Graphical WebM converter
* [ImageMagick](https://imagemagick.org/) - CLI utilities for manipulating images
* [MPD](https://www.musicpd.org/) - Music player with server-client architecture, clients to suit just about everybody's taste are available, such as ncmpc, mpc and even graphical ones

## Scripts

* [biird](https://gitlab.com/ysh16/dotfiles/-/blob/master/bin/biird) - Download images in a Twitter posts
* [naver](https://gitlab.com/ysh16/dotfiles/-/blob/master/bin/naver) - Download images in a Naver post
* [vlived](https://gitlab.com/ysh16/dotfiles/-/blob/master/bin/vlived) - V LIVE notification daemon
* [simple-webm.lua](https://gitlab.com/ysh16/dotfiles/-/blob/master/.config/mpv/scripts/simple-webm.lua) - Stupid simple webm helper mpv script, reads all its input from your
current mpv playback settings, such as start/stop from the curren A-B loop (keybind l), mute (keybind m) and subtitle visibility (keybind v). Can run directly or copy the ffmpeg command to clipboard to allow for further manual editing.

### Example usage for the biird and naver scripts

    /tmp % biird https://twitter.com/SHU_MYWAY/status/1374179089708376064
    /tmp/ExG8mw3VgAMWBtJ.jpg
    /tmp/ExG8mw_VEAAbEy_.jpg

    /tmp % naver 'https://m.post.naver.com/viewer/postView.nhn?volumeNo=27072398&memberNo=38791383'
    /tmp/IMG_2093.jpg
    /tmp/IMG_2094.jpg
    /tmp/IMG_2075.jpg
    /tmp/IMG_2061.jpg

![Shu](../EfbnjxdUYAIU0-j.jpg)
