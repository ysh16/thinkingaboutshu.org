## RSS feeds

### Services that offer feeds

* Youtube: search the channel's HTML for `application/rss` ([sfeed](https://codemadness.org/sfeed-simple-feed-parser.html) contains a program to do this for you)
* Reddit: simply place `.rss` after the subreddit name, for example: <https://old.reddit.com/r/kpop/.rss>
* Nitter (for Twitter): <https://github.com/zedeus/nitter/wiki/Instances>
* Bibliogram (for Instagram): <https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md>
* RSSHub (Weibo support, among many others): <https://github.com/DIYgod/RSSHub>
* RSSBox (SoundCloud, among others) <https://github.com/stefansundin/rssbox>

In addition, most sites such as personal blogs, online stores and git web interfaces offer feeds of their own.

### Feed aggregators

The following RSS readers suit my tastes. If they do not suit yours there is certainly a reader for you out there that is perfect for you.

#### [newsboat](https://newsboat.org/)

Crufty CLI RSS/Atom feed reader

* "execurl" scripts can be used to generate feeds from sources that don't provide them, in many cases they are fairly trivial to write. For examples in multiple programming languages please see: [https://gitlab.com/ysh16/newsboat-extensions](https://gitlab.com/ysh16/newsboat-extensions). Also see the 'Scripts and Filters' section in newsboat's manpage.
* Here are some example macros (put into `.config/newsboat/config`):
    * Open images and videos in a browser for quick downloading (useful for i.e. Bibliogram or Nitter feeds)
    * Start mpv on the linked video (useful for Youtube, Peertube,... feeds)

`macro d set external-url-viewer "awk '/(image|video)\)$/ { print $2 }' | tac | xargs $BROWSER" ; show-urls ; set external-url-viewer&`

`macro V set external-url-viewer "mpv \"$(awk '/^Link:/ { print $2; exit }')\"" ; show-urls ; set external-url-viewer&`

#### [sfeed](https://codemadness.org/sfeed-simple-feed-parser.html) and [sfeed\_curses](https://codemadness.org/sfeed_ui.html)

RSS/Atom parser and programs to format, fetch, filter, merge, order and view the feeds

* Though requiring more setup than newsboat, it is significantly more extensible, simple, and pleasant to use.
* From sfeedrc(5): "Because sfeed\_update(1) is a shellscript each function can be overridden to change its behaviour" - Below follows an example on how its fetch function can be overwritten to support the "execurl" scripts mentioned in the newsboat section.

##

    # fetch(name, url, feedfile)
    fetch() {
    	case "$2" in
    	exec:*)
    		sh -c "${2#exec:}" 2>/dev/null ;;
    	filter:*)
    		fstr=${2#filter:}
    		curl "${fstr#*:}" 2>/dev/null \
    			| sh -c "${fstr%%:*}" ;;
    	*)
    		curl "$2" 2>/dev/null ;;
    	esac
    }
    
    # list of feeds to fetch:
    feeds() {
    	# feed <name> <feedurl> [basesiteurl] [encoding]
    	feed '(G)I-DLE Tiktok' 'exec:~/newsboat-extensions/tiktok2rss.py official_gidle'
    	feed 'Shuhua Instagram' 'filter:~/newsboat-extensions/bibliogram-filter.py:https://bibliogram.snopyta.org/u/yeh.shaa_/rss.xml'
    }

![Shu](../187606921_488709462375280_6028661394842161207_n.jpg)
