## Welcome!

This website aims to be a resource containing things that are useful,
interesting and entertaining for fans of (G)I-DLE's Shuhua and K-pop
in general.

![Shu](DSC01669.jpg)

"各位鄉親父老姊妹們
有時間的話一定要一家人坐著一起吃火鍋喔
跟朋友跟心愛的人也可以
珍惜身邊擁有的人和幸福 能團聚就是幸福
但 如果是一個人的 也沒關係
人就是要學會享受孤獨！盡情享受孤獨的美吧！" 

"Folks, if you have time,
you have to sit and eat hotpot with your family, friends,
or those you love.
Cherish the people around you, happiness is being able to gather with them.
But if you're alone, that's alright as well.
You have to learn to enjoy being alone! Enjoy the beauty of being alone as much as you can!"
