## Music Show official photos

For easy downloading from the Naver blogs here, the script from
[the tools section](tools/software#scripts) can be used.

The SBS boards are more tricky and require using a fully featured browser's
developer tools.

* The Show: <https://programs.sbs.co.kr/mtv/theshow/visualboards/58358>
* Show Champion: <https://post.naver.com/my.nhn?memberNo=22171331>
* Simply K-Pop: <https://twitter.com/PLAY_K_ROUND>
* M Countdown: <https://post.naver.com/mnet_wekid>
* Music Bank: ?
* Show! Music Core: <https://post.naver.com/my.nhn?memberNo=16220685>
* Inkigayo: <https://programs.sbs.co.kr/enter/gayo/visualboards/54795>

## Music Show Youtube channels

If there is a second channel listed it usually has fancams, while the first one
has TV cuts.

* The Show:
    * <https://www.youtube.com/c/TheKPOP>
* Show Champion:
    * <https://www.youtube.com/c/ALLTHEKPOP>
* Simply K-Pop
    * <https://www.youtube.com/c/arirangworld>
* M Countdown:
    * <https://www.youtube.com/c/Mnet>
    * <https://www.youtube.com/c/MnetM2>
* Music Bank:
    * <https://www.youtube.com/c/kbsworldtv>
    * <https://www.youtube.com/user/KBSKpop>
* Show! Music Core:
    * <https://www.youtube.com/user/MBCkpop>
* Inkigayo:
    * <https://www.youtube.com/c/SBSKPOPPLAY>

![Shu](17.jpg)
