## Miscellaneous Tips

* Use Weibo's mobile site to avoid redirects to the login page: <https://m.weibo.cn>
* Some daum cafes are accessible without account by prepending 'm.' and appending '?svc': <https://m.cafe.daum.net/cube-g-i-dle?svc>
* Naver mobile page shows more categories (I think), check out those labeled HD Photo: <https://m.entertain.naver.com/photo>
* By searching YouTube for focus cams from a specific date you can get a quick idea about which groups performed. e.g. [190701 fancam](https://www.youtube.com/results?search_query=190701+%EC%A7%81%EC%BA%A0)
* You can find HD rips of some TV shows searching Twitter for 1080i, subs might be on subscene.com or reddit (e.g. r/koreanvariety)

![Shu](DSC09719.jpg)
